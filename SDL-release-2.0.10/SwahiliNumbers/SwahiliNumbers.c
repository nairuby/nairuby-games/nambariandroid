#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <SDL.h>
#include <SDL_timer.h>

int initSDL() {
 // Initialize SDL
 if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
  printf("Makosa SDL ikianza: %s\n", SDL_GetError());
  return 1;
 }
 // Print out available video drivers
 int numvideo = SDL_GetNumVideoDrivers();
 for(int i=0;i<numvideo;i++) printf("id %d driver %s\n",i,SDL_GetVideoDriver(i));
 // Print out available audio drivers
 int numaudio = SDL_GetNumAudioDrivers();
 for(int i=0;i<numaudio;i++) printf("id %d driver %s\n",i,SDL_GetAudioDriver(i));
 return 0;
}

void closeSDL(){
 // close SDL
 SDL_Quit();
}


int main(int argc, char* argv[]) {
 char *text[15] = {"Moja","Mbili","Tatu","Nne","Tano","Sita",
           "Saba","Nane","Tisa","Kumi","Sahihi","Batili",
           "Umejaribu","Ndugu, matatu ngapi?","Hesabu"};
 char *imagenumbers[10] = { "moja.bmp","mbili.bmp","tatu.bmp","nne.bmp","tano.bmp",
	                    "sita.bmp","saba.bmp","nane.bmp","tisa.bmp","kumi.bmp"};
 char numout[4];
 int  i = 2, correct=0, incorrect=0, questions = 5, num=0, texttexW=0, texttexH=0;
 int done = 0, close = 0, mouseX = 0, mouseY = 0, mousedown = 0, returncode = 0;
 time_t seconds;
 time(&seconds);
 srand(seconds);
 // initialize SDL librarires
 returncode = initSDL();
 // creates a window
 SDL_Window* win = SDL_CreateWindow("Matatu ngapi?",
                        SDL_WINDOWPOS_CENTERED,
                        SDL_WINDOWPOS_CENTERED,
                 250, 250, 0);
 // triggers the program that controls
 // your graphics hardware and sets flags
 Uint32 render_flags = SDL_RENDERER_ACCELERATED;
 // creates a renderer to render our images
 SDL_Renderer* rend = SDL_CreateRenderer(win, -1, render_flags);
 // creates a surface to load an image into the main memory
 SDL_Surface* surface;
 // Graphics hardware memory for surface
 SDL_Texture* tex[11];
 // Surface for numbers
 SDL_Surface* textsurface;
 // Texture for numbers;
 SDL_Texture* texttexture;
 // Control image position
 SDL_Rect dest[11];
 // Squares for input grid
 SDL_Rect rectgrid[10];
 // Surface for input grid
 SDL_Surface* gridsurface[10];
 // Texture for input grid
 SDL_Texture* gridtexture[10];
 // Event variable
 SDL_Event event;
 for(int k=0;k<10;k++)
 {
  // provide path to image
  surface = SDL_LoadBMP("MatatuCounter2.bmp");
  // loads image to our graphics hardware memory
  tex[k] = SDL_CreateTextureFromSurface(rend, surface);
 }
 SDL_Color color = { 255, 255, 255 };
 // question loop
 for(int j=0;j<questions;j++)
 {
  i=1+rand()%10;
  for(int k=0;k<i;k++)
  {
   // connects our texture with dest to 
   // control position
   SDL_QueryTexture(tex[k], NULL, NULL, 
                        &dest[k].w, &dest[k].h);
   // adjust height and width of our image
   dest[k].w /= 12;
   dest[k].h /= 12;
   // sets initial x-position of object
   dest[k].x = (50 + (k%5)*95 - dest[k].w) / 2 ;
   // sets initial y-position of object
   dest[k].y = (100 + ((k-(k%5))/5)*95 - dest[k].h) / 2;
  }
  // clears the screen
  SDL_RenderClear(rend);
  // write text
  for(int k=0;k<i;k++)
  {
   SDL_RenderCopy(rend, tex[k], NULL, &dest[k]);
  }
  // Make grid press points
  for(int k=0;k<10;k++)
  {
   // Red background      
   rectgrid[k].h = 45;
   rectgrid[k].w = 45;
   rectgrid[k].x = 5 + 50 * (k%5);
   rectgrid[k].y = 155 + 50 * (k/5) ;
   gridsurface[k] = SDL_CreateRGBSurface(0, rectgrid[k].w, rectgrid[k].h, 32, 0, 0, 0, 0); 
   SDL_FillRect(gridsurface[k], NULL, 
                  SDL_MapRGB(gridsurface[k]->format, 255, 0, 0));
   gridtexture[k] = SDL_CreateTextureFromSurface(rend, gridsurface[k]);
   SDL_RenderCopy(rend,gridtexture[k], NULL, &rectgrid[k]);
   // Text
   rectgrid[k].x+=10;
   rectgrid[k].y+=14;
   textsurface = SDL_LoadBMP(imagenumbers[k]);
   texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
   SDL_QueryTexture(texttexture, NULL, NULL, &rectgrid[k].w, &rectgrid[k].h);
   SDL_RenderCopy(rend,texttexture, NULL, &rectgrid[k]);
  }
  // triggers the double buffers
  // for multiple rendering
  SDL_RenderPresent(rend);
  done = 0;
  mousedown = 0;
  // Events management
  while(!done)
  {
   while(SDL_PollEvent(&event))
   {
    switch (event.type)
    {
     case SDL_QUIT:
      // handling of close button
      done = 1;
      close = 1;
      break;
     case SDL_MOUSEBUTTONDOWN:
      switch (event.button.button)
      {
       case SDL_BUTTON_LEFT:
        num = floor( mouseX / 50.0 ) + 1 + 5 * floor( ( mouseY - 150.0 ) / 50.0) ;
        mousedown=1;
        done=1;
        break;
       case SDL_BUTTON_RIGHT:
        num = floor( mouseX / 50.0 ) + 1 + 5 * floor( ( mouseY - 150.0 ) / 50.0) ;
        mousedown=1;
        done=1;
        break;
      }
     case SDL_MOUSEMOTION:
      mouseX = event.motion.x;
      mouseY = event.motion.y; 
     case SDL_KEYDOWN:
      // keyboard API for key pressed
      switch (event.key.keysym.scancode)
      {
       default:
        num = num;
      } // end switch (event.key.keysym.scancode)
    } // end switch (event.type)
    // check for events to 60 time per second
    SDL_Delay(1000 / 60 );
   } // end while(SDL_PollEvent(&event))
  } // end while(!done)
  //exit if window is closed
  if(close) break;
  // Checking keypress
  if(done)
  {
   // Checking answer      
   if(num==i)
   {
    // Say Sahihi
    correct++;
    // reset value
    num = 0;
    // animation
    for(int kk=0;kk<250;kk++)
    {
     for(int k=0;k<i;k++)
     {
      // sets  x-position of object
      dest[k].x = (50 + kk*2+ (k%5)*95 - dest[k].w) / 2 ;
      // sets y-position of object
      dest[k].y = (100 + ((k-(k%5))/5)*95 - dest[k].h) / 2;
     }
     // clears the screen
     SDL_RenderClear(rend);
     for(int k=0;k<i;k++)
     {
      SDL_RenderCopy(rend, tex[k], NULL, &dest[k]);
     }
     // triggers the double buffers
     // for multiple rendering
     SDL_RenderPresent(rend);
     SDL_Delay( 1000 / 60 );
    }
    for(int k=0;k<i;k++)
    {
     // resets initial x-position of object
     dest[k].x = (50 + (k%5)*95 - dest[k].w) / 2 ;
     // resets initial y-position of object
     dest[k].y = (100 + ((k-(k%5))/5)*95 - dest[k].h) / 2;
    }
   }else{
    // Print "Batili" and update incorrect count
    // Say Batili
    incorrect++;
    // reset value
    num = 0;
    // Pause
    SDL_Delay( 2000 );
   } // end if (num==i)
  } // end if if(event.type==SDL_KEYDOWN)
 } // end for(int j=0;j<questions;j++)
 if(!close)
 {
  // Print number of correct answers
   SDL_RenderClear(rend);
   textsurface = SDL_LoadBMP(imagenumbers[correct-1]);
   texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
   SDL_QueryTexture(texttexture, NULL, NULL, &rectgrid[correct-1].w, &rectgrid[correct-1].h);
   SDL_RenderCopy(rend,texttexture, NULL, &rectgrid[correct-1]);
   SDL_RenderPresent(rend);
   SDL_Delay( 3000 );
   SDL_RenderClear(rend);
 }
 // Destroy textures and free surfaces
 for(int k=0;k<10;k++)
 {
  SDL_DestroyTexture(tex[k]);
  SDL_DestroyTexture(gridtexture[k]);
  SDL_FreeSurface(gridsurface[k]);
 }
 SDL_DestroyTexture(texttexture);
 SDL_FreeSurface(surface);
 SDL_FreeSurface(textsurface);
 // destroy renderer
 SDL_DestroyWindow(win);
 // close SDL
 closeSDL();

 return 0;
}


